import logo from './logo.svg';
import NotesForm from './components/AddNotes.js'
import './App.css';

function App() {
  return (
    <div className="App">
      <NotesForm />
    </div>
  );
}

export default App;
