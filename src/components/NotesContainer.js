import React from 'react';
import Notes from './Notes.js';

function NotesContainer({notes, onDelete, onArchived}){
    return (
        <div className='notes-container'>
            {notes.map((note)=>(
                <Notes key={note.id} id={note.id} title={note.title} date={note.createdAt} onDelete={onDelete} onArchived={onArchived} />
            ))}
        </div>
    )
}

export default NotesContainer