import React from 'react';

class SearchBar extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            title: ''
        }
    }

    onSearchChange(event){
        this.setState(()=>{
            return {
                title: event.target.value
            }
        })
    }

    render(){
        return (
            <div className="search-bar">
                <input type="text" name="search" value={this.props.title}/>
            </div>)
    }
    
}