import React from 'react'
import axios from 'axios'

class NotesForm extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            title: '',
            body: '',
            place: '',
        }

        this.onChangeHandler = this.onChangeHandler.bind(this)
        this.onSubmitHandler = this.onSubmitHandler.bind(this)
    }

    onChangeHandler(event){
        const {name, value} = event.target
        this.setState((prevState)=>{
            return {
                ...prevState,
                [name] : value
            }
        })
    }

    onSubmitHandler(event){
        event.preventDefault()
        this.props.addNotes(this.state)

        axios.post('https://nifi.pti-cosmetics.com/general-testing',{
            name: this.state.title,
            message: this.state.body,

        })
    }

    render(){
        return (
            <div>
                <form id='note-form'>
                    <input type="text" id='notes-title' name='title' placeholder='Title' value={this.state.title} />
                    <input type="text" id='note-description' name='body' placholder='Description' value={this.state.body} />
                    <input type="text" id='test-description' name='test' placholder='test' value={this.state.place} />
                    <button type="submit">Add</button>
                </form>
            </div>)
    }
}

export default NotesForm