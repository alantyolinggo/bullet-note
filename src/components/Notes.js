import React from 'react';

function Notes({id, title, date, body, onDelete, onArchived}) {
    return (
        <div className='notes'>
            <div className='notes-title'>{title}</div>
            <div className='notes-date'>{date}</div>
            <div className='notes-body'>{body}</div>
            <div>
                <button className='btn-delete' id='btn-delete' value={id} onClick={onDelete}>Delete</button>
                <button className='btn-archived' id='btn-archived' value={id} onClick={onArchived}>Archived</button>
            </div>
        </div>
    );
}

export default Notes;